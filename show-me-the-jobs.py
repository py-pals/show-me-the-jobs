#!/usr/bin/env python
#
# Copyright 2023-2024 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from configparser import ConfigParser, ExtendedInterpolation
from inspect import isclass
from jobscraper import *
from tools import Evaluator


def all_scrapers(want_strings=True):
    keys = [key for key in globals() if isclass(globals()[key]) and issubclass(globals()[key], Scraper)]
    keys.remove("Scraper")
    return keys if want_strings else [globals()[key] for key in keys]


def sort_jobs(city_pref_desc: str, jobs: list):
    cities = city_pref_desc.split(',')
    cities += [job.location for job in jobs if job.location != '' and job.location not in cities]
    cities += ['']
    jobs = sorted(jobs, key=lambda j: (j.score, (len(cities) - cities.index(j.location)), j.date), reverse=True)
    return jobs


def main():
    text = '\n'.join([f"  {s.describe()}" for s in all_scrapers(False)])
    parser = ArgumentParser(
        formatter_class=RawDescriptionHelpFormatter,
        description="Scrapes one of the supported sites for jobs and writes them to jobs.csv.",
        epilog=f"job Scrapers:\n{text}"
    )
    parser.add_argument("-m", "--max", type=int, help="when jobs scraped equals MAX, stop")
    parser.add_argument("-q", "--quiet", action="store_true", help="suppress Scraper messages")
    parser.add_argument("scraper", type=str, choices=all_scrapers(), help="choose a job Scraper")
    args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read("user.cfg")
    if len(config.sections()) == 0:
        raise FileNotFoundError("Could not find user.cfg")
    results = globals()[args.scraper](config["scraper"], args.quiet, args.max)
    evaluator = Evaluator(config["eval.job.title"], config["eval.job.description"])
    jobs = []
    for job in results:
        score = evaluator.eval(job)
        del job.description
        job.score = score
        jobs.append(job)
    if len(jobs) == 0:
        return
    jobs = sort_jobs(config["sort_jobs"]["city_pref_desc"], jobs)
    try:
        with open("jobs.csv", 'w', encoding="utf-8") as file:
            file.write(jobs[0].csv_header() + '\n')
            for job in jobs:
                file.write(job.as_csv() + '\n')
    except Exception as e:
        raise Exception("Could not write to jobs.csv") from e


if __name__ == '__main__':
    main()
