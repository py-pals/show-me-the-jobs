#
# Copyright 2023-2024 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from configparser import ConfigParser
from contextlib import redirect_stdout
from datetime import datetime
from inspect import isclass
from io import StringIO
from jobscraper import *
from tools import Evaluator
import unittest


def build_config():
    ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
    config = ConfigParser()
    config.read_dict({
        "scraper": {
            "keyword": "Analyst",
            "user_agent": ua,
            "where": "Oregon",
            "latitude": "44.9428975",
            "longitude": "-123.0350963",
            "country": "US",
            "x-api-key": "FILL_THIS_IN [0-9A-Za-z]{40}"
        }
    })
    return config


class TestJobs(unittest.TestCase):
    def setUp(self):
        keys = [key for key in globals() if isclass(globals()[key]) and issubclass(globals()[key], Job)]
        keys.remove("Job")
        self.jobs = [globals()[key] for key in keys]

    def test_init(self):
        for job in self.jobs:
            with self.subTest(job=job):
                j = job("A Great Gig")
                self.assertEqual(j.title, "A Great Gig")

    def test_as_csv(self):
        for job in self.jobs:
            with self.subTest(job=job):
                j = job("Generic Job")
                j.location = "Salem"
                j.date = datetime(2024, 1, 23)
                j.salary = "$51627"
                j.score = 7
                j.url = "https://www.foobar.com/job/0"
                csv = j.as_csv()
                self.assertTrue(csv.startswith('"Generic Job",'))
                self.assertIn('"Salem",', csv)
                self.assertIn('"1/23/2024",', csv)
                self.assertIn('"$51627",', csv)
                self.assertIn("7,", csv)
                self.assertTrue(csv.endswith("https://www.foobar.com/job/0"))

    def test_csv_header(self):
        for job in self.jobs:
            with self.subTest(job=job):
                j = job("")
                header = j.csv_header()
                self.assertTrue(header.startswith("Title,"))
                self.assertIn("Location,", header)
                self.assertIn("Date Posted,", header)
                self.assertIn("Salary,", header)
                self.assertIn("Score,", header)
                self.assertTrue(header.endswith("URL"))


class TestScrapers(unittest.TestCase):
    def setUp(self):
        keys = [key for key in globals() if isclass(globals()[key]) and issubclass(globals()[key], Scraper)]
        keys.remove("Scraper")
        self.scrapers = [globals()[key] for key in keys]

    def test_jobs_per_page(self):
        for to_test in self.scrapers:
            with self.subTest(scraper=to_test):
                s = to_test(build_config()["scraper"])
                self.assertIsInstance(s.jobs_per_page, int)

    def test_next(self):
        for to_test in self.scrapers:
            with self.subTest(scraper=to_test):
                s = to_test(build_config()["scraper"], quiet=True)
                self.assertIsInstance(s.__next__(), Job)

    def test_quiet(self):
        for to_test in self.scrapers:
            with redirect_stdout(StringIO()) as o, self.subTest(scraper=to_test):
                s = to_test(build_config()["scraper"], quiet=True)
                s.__next__()
                self.assertEqual(o.getvalue(), "")

    def test_stop(self):
        for to_test in self.scrapers:
            with self.subTest(scraper=to_test):
                s = to_test(build_config()["scraper"], True, 2)
                [s.__next__() for x in range(2)]
                self.assertRaises(StopIteration, s.__next__)

    def test_turn_page(self):
        print()
        for to_test in self.scrapers:
            with self.subTest(scraper=to_test):
                s = to_test(build_config()["scraper"])
                s._stop = s.jobs_per_page + 1
                [s.__next__() for x in range(s._stop)]
                self.assertEqual(s.page, 2)

    def test_describe(self):
        for to_test in self.scrapers:
            with self.subTest(scraper=to_test):
                desc = to_test.describe()
                self.assertTrue(desc.startswith(to_test.__name__))
                self.assertTrue(desc[:-1].endswith(to_test.domain))


def get_dict():
    return {
        "title": {
            "IGNORECASE": "True",
            "re0": ".*"
        },
        "description": {
            "IGNORECASE": "True",
            "re0": ".*"
        }
    }


class TestEvaluator(unittest.TestCase):
    def test_init(self):
        config = ConfigParser()
        config.read_dict(get_dict())
        Evaluator(config["title"], config["description"])

    def test_bad_re(self):
        config = ConfigParser()
        config.read_dict({
            "title": {
                "re0": "\\"
            },
            "description": {
                "re0": ".*"
            }
        })
        with self.assertRaises(ValueError):
            Evaluator(config["title"], config["description"])

    def test_eval(self):
        class MockJob(Job):
            def __init__(self, title):
                super().__init__()
                self.title = title
        
            def as_csv(self) -> str:
                return ""
        
            def csv_header(self) -> str:
                return ""
        
        tl = "self.title_re["
        tr = "].search(job.title)"
        dl = "self.description_re["
        dr = "].search(job.description)"
        a = MockJob("Analyst")
        a.description = "Foobar"
        b = MockJob("Sr. Scribe")
        c = MockJob("Intern")
        c.description = "Bar"
        to_read = get_dict()
        to_read["title"]["re0"] = "(Senior|Sr[ .])"
        to_read["title"]["re1"] = "Intern"
        to_read["title"]["exec0"] = f"score -= 2 if {tl}0{tr} else (1 if {tl}1{tr} else 0)"
        to_read["description"]["re0"] = "Foo"
        to_read["description"]["re1"] = "Bar"
        to_read["description"]["exec0"] = "score += "
        to_read["description"]["exec0"] += f"2 if {dl}0{dr} and {dl}1{dr} else (1 if {dl}0{dr} or {dl}1{dr} else 0)"
        config = ConfigParser()
        config.read_dict(to_read)
        e = Evaluator(config["title"], config["description"])
        self.assertEqual(e.eval(a), 2)
        self.assertEqual(e.eval(b), -2)
        self.assertEqual(e.eval(c), 0)


if __name__ == '__main__':
    unittest.main()
