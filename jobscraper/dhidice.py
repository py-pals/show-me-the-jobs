#
# Copyright 2023-2024 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from bs4 import BeautifulSoup
from configparser import SectionProxy
from datetime import date, timedelta
import html as HTML
import re
from urllib.error import HTTPError
from .scraper import Job, Scraper


class DiceJob(Job):
    """This class stores data extracted by the DhiDice Scraper."""

    def __init__(self, title):
        super().__init__()
        self.title = title
        self.company = ''

    def as_csv(self) -> str:
        posted = f"{self.date.month}/{self.date.day}/{self.date.year}"
        return f'"{self.title}","{self.company}","{self.location}","{posted}","{self.salary}",{self.score},{self.url}'

    def csv_header(self) -> str:
        return "Title,Company,Location,Date Posted,Salary,Score,URL"


class DhiDice(Scraper):
    """This class scrapes listings from Dice.com's job search."""
    domain = "www.dice.com"

    @property
    def jobs_per_page(self) -> int:
        return 20

    def __init__(self, config: SectionProxy, quiet: bool = False, stop=None):
        """Instantiate a DhiDice Scraper.
        :param config: Configuration details (must include keyword and user_agent)
        :param quiet: When True (default: False), Scraper messages are suppressed.
        :param stop: When >= 1 (default: None), ends scraping once {stop} jobs are returned.
        """
        super().__init__(config["keyword"], r'job-detail/(?P<id>[0-9a-f-]+)"', config["user_agent"], quiet, stop)
        self.base = f"https://{self.domain}/"
        self.query = f"https://job-search-api.svc.dhigroupinc.com/v1/dice/jobs/search?q={self.job_kw}"
        self.query += f"&countryCode2={config['country']}&latitude={config['latitude']}&locationPrecision=City&fj=true"
        self.query += f"&longitude={config['longitude']}&radius=100&radiusUnit=mi&pageSize=20&facets=employmentType"
        self.query += "%7CpostedDate%7CworkFromHomeAvailability%7CemployerType%7CeasyApply%7CisRemote&culture=en"
        self.query += "&fields=id%7CdetailsPageUrl&filters.employmentType=FULLTIME%7CPARTTIME&includeRemote=true"
        self.query += "&interactionId=0&recommendations=true&page="
        self._user_agent["x-api-key"] = config["x-api-key"]

    def __next__(self):
        self.start_next()
        if self.x % self.jobs_per_page == 1:
            try:
                html = self.url_open_read_decode(self.query + str(self.page))
                if self.x == 1:
                    self.init_results(html, r'"totalResults":(?P<hits>\d+)')
                self.turn_page(html)
            except Exception:
                raise StopIteration
        jid = self.next_id_match().group("id")
        url = f"{self.base}job-detail/{jid}"
        if jid in self.ids:
            self.print(f"Skipping duplicate ({url})")
            return self.__next__()
        else:
            self.ids.append(jid)
            try:
                page = BeautifulSoup(self.url_open_read_decode(url), "html.parser")
                job = DiceJob(HTML.unescape(page.select('[data-cy="jobTitle"]')[0].text))
                company = page.select('[data-cy="companyNameLink"]')
                if len(company) > 0:
                    job.company = company[0].text
                location = page.select('[class~="job-header_jobDetail__ZGjiQ"]')[0].text
                if " ago " in location:
                    location = ""
                job.location = location.split(',')[0] if ',' in location else location
                age = page.select('[id="timeAgo"]')
                today = date.today()
                if len(age) > 0:
                    age_s = age[0].text.replace("60+", "61")
                    job.date = today
                    if " ago " in age_s:
                        try:
                            age_i = int(age_s.split(' ')[1])
                            job.date = today - timedelta(age_i)
                        except ValueError:
                            pass
                spans = page.find_all("span")
                spans = [spans[x].text.replace('$', "").replace("USD", "").split(" per ")[0] for x in range(3, 5)]
                pay_re = re.compile(r"[0-9,.]{2,}(\+| - )")
                match = pay_re.search(spans[0])
                if not match:
                    match = pay_re.search(spans[1])
                if match:
                    pay = match.string.replace(' ', "")
                    while pay[-1].isalpha():
                        pay = pay[:-1]
                    job.salary = pay
                job.url = url
                job.description = HTML.unescape(page.select('[data-testid="jobDescriptionHtml"]')[0].text)
                return job
            except HTTPError:
                return self.__next__()

    @classmethod
    def describe(cls) -> str:
        return f"DhiDice ({cls.domain})"
