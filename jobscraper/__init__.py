from .scraper import Job, Scraper
from .dhidice import DhiDice, DiceJob
from .oregonjobs import OregonJob, OregonJobs